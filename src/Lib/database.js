export const db = new PouchDB("SvelteCRUD");


db.get("_design/filters").catch(() => {
    console.info("Setting Up DB with filter design doc")
    db.put({
        _id: "_design/filters",
        filters: {
            doc_type: "function(doc, req) {\n    return (doc.type == req.query.type);\n}",
            starts_with: "function(doc, req)\n{\n  if(doc._id.startsWith(req.query.id)) {\n    return true;\n  }\n\n  return false;\n}"
            }
    })
});