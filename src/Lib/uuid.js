export default function uuid(timestamp) {
    if (!timestamp) {
        timestamp = Date.now()
    }

    return timestamp.toString(36) + Math.floor(Math.random() * 4228250625).toString(36)
}