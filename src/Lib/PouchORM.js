import { db } from './database';
import { uuid } from './utils';

import { readable } from 'svelte/store';
import { sortBy } from 'lodash-es';

import { buildLexoRank, NumeralSystem64 } from "@wewatch/lexorank";

window.LexoRank = buildLexoRank({
    NumeralSystem: NumeralSystem64,
    maxOrder: 8,
    initialMinDecimal: "10000",
    defaultGap: "10000"
})

class GenericField {
    constructor(id) {
        this.id = id;

        this.details = {
            _id: id
        };
        this._children = [];
        this.rank = LexoRank.middle()

        this.proxy = {
            get: function (target, prop) {
                if (prop in target) {
                    return target[prop];
                } else {
                    if (prop.startsWith("_")) {
                        throw new Error("Cannot create properties that start with _ ");
                    }

                    const newProp = new GenericField(`${target.id}:${prop}`);
                    target[prop] = new Proxy(newProp, newProp.proxy);
                    return target[prop];
                }
            },

            set: function (target, prop, value) {
                target[prop] = value;

                return true
            }
        }
    }

    save(data) {
        if (data._id !== this.id) {
            throw new Error("Invalid ID for save");
        }

        return db.put(data);
    }

    addChild(doc) {
        if (doc._id) {
            throw "Cannot create new doc with existing ID";
        }

        const child_id = uuid();
        doc._id = `${this.id}:_child:${child_id}`;
        doc.child_id = child_id;

        doc.rank_ = this.rank.toString();
        this.rank = this.rank.genNext();

        return db.put(doc);
    }

    [Symbol.iterator]() {
        let idx = -1;

        return {
            next: () => ({value: this._children[++idx], done: !(idx in this._children)})
        }
    }

    delete() {
        return db.allDocs({
            include_docs: true,
            startkey: this.id,
            endkey: `${this.id}:\uFFF0`
        }).then((result) => {
            let docs_to_delete = [];
            for (let row of result.rows) {
                row.doc._deleted = true;
                docs_to_delete.push(row.doc);
            }

            // delete docs in reverse so
            // change updates arrive in order
            docs_to_delete.reverse()

            return db.bulkDocs(docs_to_delete);
        })
    }

    moveChild(child, after,  before) {
        let beforeRank, afterRank;
        let newRank;
        if (!before) {
            // add to the end of the list
            newRank = this.rank.toString();
            this.rank = this.rank.genNext();
        } else {
            if (!after) {
                afterRank = LexoRank.min()
            } else {
                afterRank = LexoRank.parse(after.details.rank_);
            }

            beforeRank = LexoRank.parse(before.details.rank_);

            newRank = afterRank.between(beforeRank).toString();
        }

        child.details.rank_ = newRank;

        return child.save(child.details);
    }

    handleDoc(doc, r) {
        let [id, ...rest] = r;

        if (rest.length === 0) {
            this.details = doc;
        } else if (id === "_child") {
            let id;
            [id, ...rest] = rest;

            let child_id = `${this.id}:_child:${id}`;
            let idx = this._children.length - 1;

            // handle rank order
            if (doc._id == child_id && doc.rank_) {
                if (doc.rank_ >= this.rank.toString()) {
                    this.rank = this.rank.genNext();
                }
            }

            if (idx === -1) {
                const child = new GenericField(child_id);
                child.handleDoc(doc, rest);
                this._children.push(new Proxy(child, child.proxy));
            } else {

                if (doc._deleted && rest.length == 0) {
                    this._children = this._children.filter(d => d.id != doc._id);

                    return;
                }

                // new way of handling items
                let found = false;
                for (let i = 0; i < this._children.length; i++) {
                    if (this._children[i].id == child_id) {
                        this._children[i].handleDoc(doc, rest);
                        found = true
                        break
                    }
                }

                if (!found) {
                    const child = new GenericField(child_id);
                    child.handleDoc(doc, rest);
                    this._children.push(new Proxy(child, child.proxy));
                }

            }

            this._children = sortBy(this._children, c => c.details.rank_ ? c.details.rank_ : c.id);

        } else {
            this.proxy.get(this, id).handleDoc(doc, rest);
        }
    }
}
export default class ORM {
    static modelName = "";

    static createItem (details) {
        const id = uuid();
        return db.put({
            _id: `${this.modelName}:${id}`,
            item_id: id,
            type: this.modelName,
            ...details
        })
    }

    static save(details) {
        if (!details._id.startsWith(this.modelName)) {
            throw new error("Invalid ID for ORM Type");
        }

        return db.put(details);
    }

    static delete(id) {
        if (!id.startsWith(this.modelName)) {
            throw new error("Invalid ID for ORM Type");
        }

        return db.allDocs({
            include_docs: true,
            startkey: id,
            endkey: `${id}:\uFFF0`
        }).then((result) => {
            let docs_to_delete = [];
            for (let row of result.rows) {
                row.doc._deleted = true;
                docs_to_delete.push(row.doc);
            }

            return db.bulkDocs(docs_to_delete);
        })
    }

    static getList() {
        let item_list = [];

        return readable(undefined, (set) => {
            db.find({
                selector: {
                    type: this.modelName
                }
            }).then(results => {
                item_list = results.docs;
                set(item_list)
            });

            let changeMonitor = db.changes({
                since: "now",
                live: true,
                include_docs: true,
                filter: (doc) => doc.type.startsWith(this.modelName),
            }).on("change", (change) => {
                if (change.deleted) {
                    item_list = item_list.filter(item => item._id != change.id)
                } else {
                    let found = false;
                    item_list = item_list.map(item => {
                    if (item._id != change.id) return item;
                        found = true;
                        return change.doc
                    })

                    if (!found) {
                        item_list.push(change.doc);
                    }
                }

                set(item_list);
            })

            return () => {
                changeMonitor.cancel();
            }
        });
    }

    static async getStatic(id) {
        if (!id.startsWith(this.modelName)) {
            throw new error("Invalid ID for ORM Type");
        }

        let _RootField = new GenericField(id);
        let RootField = new Proxy(_RootField, _RootField.proxy);

        let result = await db.allDocs({
            include_docs: true,
            startkey: `${id}`,
            endkey: `${id}:\ufff0`
        })

        for (let row of result.rows) {
            let [...r] = row.id.slice(id.length + 1).split(":");
            RootField.handleDoc(row.doc, r);
        }

        return RootField;
    }

    static getStore(id) {
        if (!id.startsWith(this.modelName)) {
            throw new error("Invalid ID for ORM Type");
        }

        let _RootField = new GenericField(id);
        let RootField = new Proxy(_RootField, _RootField.proxy);

        return readable(undefined, (set) => {
            db.allDocs({
                include_docs: true,
                startkey: `${id}`,
                endkey: `${id}:\ufff0`
            }).then(result => {
                for (let row of result.rows) {
                    let [...r] = row.id.slice(id.length + 1).split(":");
                    RootField.handleDoc(row.doc, r);
                }
                set(RootField);
            });

            let changeMonitor = db.changes({
                since: 'now',
                live: true,
                include_docs: true,
                filter: (doc) => doc._id.startsWith(id),
            }).on("change", (change) => {
                let [...r] = change.id.slice(id.length + 1).split(":");

                RootField.handleDoc(change.doc, r);

                set(RootField);
            });

            return () => {
                changeMonitor.cancel();
            };
        })
    }
}