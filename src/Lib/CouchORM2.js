import { db } from './database';
import { readable } from 'svelte/store';
import sortBy from 'lodash-es/sortBy';
import uuid from './uuid';
import { buildLexoRank, NumeralSystem64 } from "@wewatch/lexorank";

export const LexoRank = buildLexoRank({
    NumeralSystem: NumeralSystem64,
    maxOrder: 8,
    initialMinDecimal: "10000",
    defaultGap: "10000"
})

export class GenericField {
    constructor(id, custom_db) {
        this.db = custom_db;

        this.id = id;

        this.details = {
            _id: id
        };

        this._children = [];
        this._rank = LexoRank.middle();

        this.subscribers = [];

        this.proxy = {
            get: function (target, prop) {
                // add _ as shortcut for details
                if (prop == "_") {
                    return target.details;
                }

                if (prop in target) {
                    return target[prop];
                } else {
                    if (prop.startsWith("_")) {
                        throw new Error("Cannot create properties that start with _ ");
                    }

                    const newProp = new GenericField(`${target.id}:${prop}`, custom_db);
                    target[prop] = new Proxy(newProp, newProp.proxy);
                    return target[prop];
                }
            },

            set: function (target, prop, value) {
                target[prop] = value;

                return true
            }
        }
    }

    subscribe(fn) {
        fn({
            details: this.details,
            children: this._children
        });
        this.subscribers.push(fn);

        return () => {
            this.subscribers = this.subscribers.filter(f => f !== fn)
        };
    }

    notify() {
        for (let fn of this.subscribers) {
            fn({
                details: this.details,
                children: this._children
            });
        }
    }

    dump() {
        let data = {};

        for (let prop in this) {
            if (!prop.startsWith("_") && prop !== "proxy") {
                if (this[prop].dump) {
                    data[prop] = this[prop].dump();
                } else {
                    data[prop] = this[prop];
                }
            }
        }

        for (let child of this._children) {
            if (!data['children']) data['children'] = [];
            data['children'].push(child.dump());
        }

        return data;
    }

    save(doc) {
        if (doc._id !== this.id) {
            throw new Error("Invalid ID for save");
        }

        return this.db.put(doc);
    }

    addChild(doc) {
        // create a copy so we don't mutate the original object
        doc = JSON.parse(JSON.stringify(doc))

        if (doc._id) {
            throw new Error("Cannot create new doc with existing ID");
        }

        const child_id = uuid();
        doc._id = `${this.id}:_child:${child_id}`;
        doc.child_id = child_id;

        doc.rank_ = this._rank.toString();
        this._rank = this._rank.genNext();

        return this.db.put(doc);
    }

    [Symbol.iterator]() {
        let idx = -1;

        return {
            next: () => ({value: this._children[++idx], done: !(idx in this._children)})
        }
    }

    delete() {
        return this.db.allDocs({
            include_docs: true,
            startkey: this.id,
            endkey: `${this.id}:\uFFF0`
        }).then((result) => {
            let docs_to_delete = [];
            for (let row of result.rows) {
                row.doc._deleted = true;
                docs_to_delete.push(row.doc);
            }

            // delete docs in reverse so
            // change updates arrive in order
            docs_to_delete.reverse()
            return this.db.bulkDocs(docs_to_delete);
        })
    }

    async moveChildToNewParent(child, old_parent_id, new_parent_id, after, before) {
        let beforeRank, afterRank;
        let newRank;

        if (!before) {
            // add to the end of the list
            newRank = this._rank.toString();
            this._rank = this._rank.genNext();
        } else {
            if (!after) {
                afterRank = LexoRank.min()
            } else {
                afterRank = LexoRank.parse(after.details.rank_);
            }

            beforeRank = LexoRank.parse(before.details.rank_);

            newRank = afterRank.between(beforeRank).toString();
        }

        let updated_docs = [];

        let new_doc = JSON.parse(JSON.stringify(child.details));

        delete new_doc._rev;
        new_doc._id = new_doc._id.replace(old_parent_id, new_parent_id);
        new_doc.rank_ = newRank;

        updated_docs.push(new_doc);

        child.details._deleted = true;
        updated_docs.push(child.details);


        let docs = await this.db.allDocs({
            include_docs: true,
            startkey: `${child.id}:`,
            endkey: `${child.id}:\uFFF0`
        })

        for (let row of docs.rows) {
            let doc = row.doc;

            let new_doc = JSON.parse(JSON.stringify(doc));
            new_doc._id = new_doc._id.replace(old_parent_id, new_parent_id);
            delete new_doc._rev;
            updated_docs.push(new_doc);

            doc._deleted = true;
            updated_docs.push(doc);
        }

        return this.db.bulkDocs(updated_docs);
    }

    async moveChild(child, after,  before) {
        let beforeRank, afterRank;
        let newRank;

        // check if rank is missing
        if (!child.details.rank_) {
            // add rank to all children
            let docs_to_add = []
            for (let c of this._children) {
                c.details.rank_ = this._rank.toString();
                this._rank = this._rank.genNext();
                docs_to_add.push(c.details);
            }

            let updated_docs = await this.db.bulkDocs(docs_to_add);
            for (let doc of updated_docs) {
                if (doc.id == child.id) {
                    child.details._rev = doc.rev;
                }
            };
        }


        if (!before) {
            // add to the end of the list
            newRank = this._rank.toString();
            this._rank = this._rank.genNext();
        } else {
            if (!after) {
                afterRank = LexoRank.min()
            } else {
                afterRank = LexoRank.parse(after.details.rank_);
            }

            beforeRank = LexoRank.parse(before.details.rank_);

            newRank = afterRank.between(beforeRank).toString();
        }

        child.details.rank_ = newRank;

        return child.save(child.details);
    }

    handleDoc(doc, r) {
        let [id, ...rest] = r;

        if (!id && rest.length === 0) {
            this.details = doc;
            this.notify();
        } else if (id === "_child") {
            [id, ...rest] = rest;

            let child_id = `${this.id}:_child:${id}`;
            let idx = this._children.length - 1;

            // handle rank order
            if (doc._id == child_id && doc.rank_) {
                if (doc.rank_ >= this._rank.toString()) {
                    this._rank = LexoRank.parse(doc.rank_).genNext();
                }
            }

            if (idx === -1) {
                const child = new GenericField(child_id, this.db);
                child.handleDoc(doc, rest);

                // don't add to children on delete
                if (!doc._deleted) {
                    this._children.push(new Proxy(child, child.proxy));
                    this.notify()
                }
            } else {

                // new way of handling items
                let found = false;
                for (let i = 0; i < this._children.length; i++) {
                    if (this._children[i].id == child_id) {
                        this._children[i].handleDoc(doc, rest);
                        found = true
                        break
                    }
                }

                if (!found) {
                    const child = new GenericField(child_id, this.db);
                    child.handleDoc(doc, rest);

                    // don't add to children if processing a delete
                    if (!doc._deleted) {
                        this._children.push(new Proxy(child, child.proxy));
                        this.notify();
                    }
                }

            }

            // We only need to handle sorts and deletions when a direct child has changed
            if (rest.length === 0) {
                // remove deleted children
                this._children = this._children.filter(c => !c.details._deleted)

                // sort children by rank
                this._children = sortBy(this._children, c => [c.details.rank_ ? c.details.rank_ : c.id, c.id]);
                this.notify();
            }
        } else {
            this.proxy.get(this, id).handleDoc(doc, rest);
        }
    }
}
export class ORM {
    static modelName = "";

    constructor (custom_db) {
        if (custom_db) {
            this.db = custom_db
        } else {
            this.db = db
        }
    }

    createItem (details, id) {
        if (!id) {
            id = uuid();
        }
        return this.db.put({
            _id: `${this.constructor.modelName}:${id}`,
            item_id: id,
            type: this.constructor.modelName,
            ...details
        })
    }

    save(doc) {
        if (!doc._id.startsWith(this.constructor.modelName)) {
            throw new error(`Invalid ID for ORM Type ${this.constructor.modelName}: ${doc._id}`)
        }
        return this.db.put(doc);
    }

    getDetails(id) {
        return this.db.get(id);
    }

    delete(id) {
        if (!id.startsWith(this.constructor.modelName)) {
            throw new error("Invalid ID for ORM Type");
        }

        return this.db.allDocs({
            include_docs: true,
            startkey: id,
            endkey: `${id}:\uFFF0`
        }).then((result) => {
            let docs_to_delete = [];
            for (let row of result.rows) {
                row.doc._deleted = true;
                docs_to_delete.push(row.doc);
            }

            docs_to_delete.reverse()

            return this.db.bulkDocs(docs_to_delete);
        })
    }

    getList() {
        let item_list = [];

        return readable(undefined, (set) => {
            this.db.find({
                selector: {
                    type: this.constructor.modelName
                }
            }).then(results => {
                item_list = results.docs;
                set(item_list)
            });

            let changeMonitor = this.db.changes({
                since: "now",
                live: true,
                include_docs: true,
                filter: "filters/doc_type",
                query_params: {
                    type: this.constructor.modelName
                }
            }).on("change", (change) => {
                if (change.deleted) {
                    item_list = item_list.filter(item => item._id != change.id)
                } else {
                    let found = false;
                    item_list = item_list.map(item => {
                    if (item._id != change.id) return item;
                        found = true;
                        return change.doc
                    })

                    if (!found) {
                        item_list.push(change.doc);
                    }
                }

                set(item_list);
            })

            return () => {
                changeMonitor.cancel();
            }
        });
    }

    async getStatic(id) {
        if (!id.startsWith(this.constructor.modelName)) {
            throw new error("Invalid ID for ORM Type");
        }

        let _RootField = new GenericField(id, this.db);
        let RootField = new Proxy(_RootField, _RootField.proxy);

        let result = await this.db.allDocs({
            include_docs: true,
            startkey: `${id}`,
            endkey: `${id}:\ufff0`
        })

        if (!result.rows.length) {
            throw new Error("Not Found")
        }

        for (let row of result.rows) {
            let [...r] = row.id.slice(id.length + 1).split(":");
            RootField.handleDoc(row.doc, r);
        }

        return RootField;
    }

    getStore(id) {
        if (!id.startsWith(this.constructor.modelName)) {
            throw new Error(`Invalid ID for ORM Type: ${id}`);
        }

        let _RootField = new GenericField(id, this.db);
        let RootField = new Proxy(_RootField, _RootField.proxy);

        return readable(RootField, (set) => {
            this.db.allDocs({
                include_docs: true,
                startkey: `${id}`,
                endkey: `${id}:\ufff0`
            }).then(result => {
                for (let row of result.rows) {
                    let [...r] = row.id.slice(id.length + 1).split(":");
                    RootField.handleDoc(row.doc, r);
                }
                set(RootField);
            });

            let changeMonitor = this.db.changes({
                since: 'now',
                live: true,
                include_docs: true,
                filter: "filters/starts_with",
                query_params: {
                    id: id
                }
            }).on("change", (change) => {
                let [...r] = change.id.slice(id.length + 1).split(":");

                RootField.handleDoc(change.doc, r);
            });

            return () => {
                changeMonitor.cancel();
            };
        });
    }
}