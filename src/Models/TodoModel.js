import { ORM } from '../Lib/CouchORM2';

class TodoModel extends ORM {
    static modelName = "ReportTemplate";

    static fields = [
        {
            name: "name",
            label: "List Name",
            required: true,
            type: "text"
        }
    ]
}

const Todo = new TodoModel();

export default Todo;

/*

import ORM from '../Lib/PouchORM';

export default class Todo extends ORM {
    static modelName = "Todo";

    static ListFields = [
        {
            name: "name",
            label: "List Name",
            required: true,
            type: "text"
        }
    ]

    static ItemFields = [
        {
            name: "name",
            label: "Item Name",
            required: true,
            type: "text"
        },
        {
            name: "done",
            label: "Completed",
            type: "checkbox"
        }
    ]
}

*/